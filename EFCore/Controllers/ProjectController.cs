﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using EFCore.BLL.Services;
using EFCore.Common.DTOs.Project;
using System.Threading.Tasks;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/projects/[action]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly ProjectService _projectService;
        public ProjectController(ProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<IEnumerable<ProjectDTO>> Get()
        {
            return await _projectService.GetProjects();
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<ProjectDetailsDTO> GetProjectDetails(int id)
        {
            return await _projectService.GetProjectDetails(id);
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> Create([FromBody] ProjectCreateDTO dto)
        {
            return Ok(await _projectService.CreateProject(dto));
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] ProjectDTO dto)
        {
            await _projectService.UpdateProject(dto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _projectService.DeleteProject(id);
            return NoContent();
        }

        [Route("{id}")]
        [HttpGet]
        public async Task<ProjectDTO> GetProject(int id)
        {
            var project = await _projectService.GetProject(id);

            return project;
        }

    }
}