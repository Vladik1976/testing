﻿
namespace EFCore.Common.Enumerators
{
    public enum TaskState
    {
        ToDo=0,
        InProgress,
        Done,
        Canceled
    }
}
