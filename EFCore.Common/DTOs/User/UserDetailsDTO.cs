﻿using EFCore.Common.DTOs.Project;
using EFCore.Common.DTOs.Task;

namespace EFCore.Common.DTOs.User
{
    public class UserDetailsDTO
    {
        public UserDTO User { get; set; }

        public ProjectDTO Project { get; set; }

        public int? NumberOfTasks { get; set; }

        public int UnfinishedTasks { get; set; }

        public TaskDTO LongestTask { get; set; }
    }
}
