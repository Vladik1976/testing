﻿using EFCore.Common.DTOs.Task;
using System.Collections.Generic;

namespace EFCore.Common.DTOs.User
{
    public class UserTasksDTO
    {
        public string FirstName { get; set; }

        public List<TaskDTO> Tasks { get; set; }
    }
}
