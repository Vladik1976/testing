﻿

using EFCore.Common.DTOs.Task;

namespace EFCore.Common.DTOs.Project
{
    public class ProjectDetailsDTO
    {
        public ProjectDTO Project { get; set; }

        public TaskDTO LongestTask { get; set; }

        public TaskDTO ShortestTask { get; set; }

        public int NumberOfPerformers { get; set; }

    }
}