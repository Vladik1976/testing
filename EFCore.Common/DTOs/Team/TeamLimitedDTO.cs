﻿using EFCore.Common.DTOs.User;
using System.Collections.Generic;


namespace EFCore.Common.DTOs.Team
{
    public class TeamLimitedDTO
    {
        public int TeamId { get; set; }

        public string TeamName { get; set; }

        public ICollection<UserDTO> TeamUsers { get; set; }
    }
}
