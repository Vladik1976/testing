﻿using EFCore.DAL.Entities.Abstract;
using System;
using System.ComponentModel.DataAnnotations;

namespace EFCore.DAL.Entities
{
    public class Task : BaseEntity
    {
       public Task() { }

        [Required]
        public int? ProjectId { get; set; }
        public Project Project { get; set; }
        
        [Required]
        public int? PerformerId { get; set; }

        public User Performer { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public int State { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
