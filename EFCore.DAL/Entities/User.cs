﻿using EFCore.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EFCore.DAL.Entities
{
    public class User:BaseEntity
    {
        public User()
        {
            Tasks = new List<Task>();
            Projects = new List<Project>();
        }
        public int? TeamId { get; set; }
        public Team Team { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public DateTime? RegisteredAt { get; set; }

        public DateTime? Birthday { get; set; }

        public ICollection<Task> Tasks { get; set; }
        public ICollection<Project> Projects { get; set; }
    }
}
