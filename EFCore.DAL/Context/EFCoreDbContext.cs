﻿using EFCore.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace EFCore.DAL.Context
{
    public class EFCoreDbContext : DbContext
    {
        public EFCoreDbContext(DbContextOptions<EFCoreDbContext> options)
            : base(options) { }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasMany(u=>u.Tasks)
                .WithOne(t=>t.Performer)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Team>()
                .HasMany(u => u.Users)
                .WithOne(t => t.Team)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<User>()
                .HasMany(u => u.Projects)
                .WithOne(p => p.Author)
                .OnDelete(DeleteBehavior.Restrict);

        //Seed
            var teams = new List<Team>
            {
                new Team{Id=1,Name="Clever team",CreatedAt=DateTime.Now}
            };

            var users = new List<User>
            {
                new User{Id=1,FirstName="John",LastName="Doe",Email="johndoe@gmail.com",TeamId=1,
                        RegisteredAt=DateTime.Now,Birthday=DateTime.Now.AddYears(-10)},

                new User{Id=2,FirstName="Bill",LastName="Gates",Email="billy@microsoft.com",
                        RegisteredAt=DateTime.Now,Birthday=DateTime.Now.AddYears(-10).AddMonths(-3)},

                new User{Id=3,FirstName="Entony",LastName="Brown",Email="ent@brown.com",TeamId=1,
                        RegisteredAt=DateTime.Now,Birthday=DateTime.Now.AddYears(-10).AddMonths(-4).AddDays(5)},

                new User{Id=4,FirstName="Lazy",LastName="Cat",Email="haha@cake.com",
                        RegisteredAt=DateTime.Now,Birthday=DateTime.Now.AddYears(-10).AddDays(7)},
            };

            var projects = new List<Project>
            {
                new Project{Id=1,AuthorId=2,Name="My first Project",Description="Good Description for the first project",TeamId=1,
                        CreatedAt=DateTime.Now,Deadline=DateTime.Now.AddDays(8)},

                new Project{Id=2,AuthorId=4,Name="a botle of Milk",Description="and nothing more...",
                        CreatedAt=DateTime.Now,Deadline=DateTime.Now.AddDays(5)}
            };

            var tasks = new List<Task>
            {
                new Task{Id=1,Name="Nice task for the First Project",Description="you must do something",
                        PerformerId=3,ProjectId=1,State=0,CreatedAt=DateTime.Now},

                new Task{Id=2,Name="Just dummy task for the seeding in my HomeWork",Description="No comments...",
                        PerformerId=4,ProjectId=2,State=0}
            };

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);

            base.OnModelCreating(modelBuilder);
        }

    }
}
