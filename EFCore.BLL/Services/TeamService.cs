﻿using AutoMapper;
using EFCore.BLL.Services.Abstract;
using EFCore.Common.DTOs.Team;
using EFCore.DAL.Context;
using EFCore.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCore.BLL.Services
{
    public class TeamService :BaseService
    {
        public TeamService(IMapper mapper, EFCoreDbContext context) : base(mapper,context){}
        /// <summary>
        /// Get list of teams contains user over 10 years old
        /// </summary>
        /// <returns></returns>
        public async Task<List<TeamLimitedDTO>> GetTeamsLimited()
        {
            var teams = (from t in await GetTeams()
                               where t.Users.All(u => DateTime.Now.Year - u.Birthday.Value.Year > 10)
                               select new TeamLimitedDTO
                               {
                                   TeamId = t.Id,
                                   TeamName = t.Name,
                                   TeamUsers = t.Users.OrderByDescending(u => u.RegisteredAt).ToList()
                               });

            return teams.ToList();

        }

        public async Task<ICollection<TeamDTO>> GetTeams()
        {

            var teams = await _context.Teams
               .Include(team => team.Users)
               .ToListAsync();

            return _mapper.Map<ICollection<TeamDTO>>(teams);
        }

        public async Task<TeamDTO> CreateTeam(TeamCreateDTO teamDTO)
        {
            var teamEntity = _mapper.Map<Team>(teamDTO);
            teamEntity.CreatedAt = System.DateTime.Now;

            _context.Add(teamEntity);
            await _context.SaveChangesAsync();

            var createdTeam = await _context.Teams
               .FirstAsync(team => team.Id == teamEntity.Id);

            var createdTeamDTO = _mapper.Map<TeamDTO>(createdTeam);

            return createdTeamDTO;
        }
        public async System.Threading.Tasks.Task UpdateTeam(TeamDTO teamDTO)
        {
            var teamEntity = await GetTeamByIdInternal(teamDTO.Id);

            if (teamEntity != null)
            {
                teamEntity.Name = teamDTO.Name;

                _context.Teams.Update(teamEntity);
                await _context.SaveChangesAsync();
            }
        }

        public async System.Threading.Tasks.Task DeleteTeam(int id)
        {
            var teamEntity = await GetTeamByIdInternal(id);
            if (teamEntity != null)
            {
                _context.Teams.Remove(teamEntity);
                await _context.SaveChangesAsync();
            }
        }

        public async System.Threading.Tasks.Task<TeamDTO> GetTeam(int id)
        {
            var teamEntity = await GetTeamByIdInternal(id);
            if (teamEntity != null)
            {
                return _mapper.Map<TeamDTO>(teamEntity);
            }
            else
            {
                return new TeamDTO();
            }
        }

        private async Task<Team> GetTeamByIdInternal(int id)
        {
            return await _context.Teams
              .Include(team => team.Users)
              .FirstOrDefaultAsync(team => team.Id == id);

        }
    }
}
