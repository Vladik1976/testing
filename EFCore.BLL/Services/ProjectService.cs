﻿using AutoMapper;
using EFCore.Common.DTOs.Project;
using EFCore.BLL.Services.Abstract;
using EFCore.DAL.Context;
using EFCore.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EFCore.BLL.Services
{
    public class ProjectService: BaseService
    {
        public ProjectService(IMapper mapper,EFCoreDbContext context) : base(mapper,context){}

        public async Task<ProjectDetailsDTO> GetProjectDetails(int id)
        {
            var projectEntity = await GetProjectByIdInternal(id);

            return _mapper.Map<ProjectDetailsDTO>(projectEntity);



        }
        public async Task <ICollection<ProjectDTO>> GetProjects() {

            var projects = await _context.Projects
               .Include(project => project.Author)
               .Include(project => project.Tasks)
               .ThenInclude(task => task.Performer)
               .Include(project => project.Team)
               .ToListAsync();

            return _mapper.Map<ICollection<ProjectDTO>>(projects);
        }

        public async Task<ProjectDTO> GetProject(int id)
        {

            var projectEntity = await GetProjectByIdInternal(id);
            if (projectEntity != null)
            {
                return _mapper.Map<ProjectDTO>(projectEntity);
            }
            else
            {
                return new ProjectDTO();
            }
        }

        public async Task<ProjectDTO> CreateProject(ProjectCreateDTO projectDTO)
        {
            var projectEntity = _mapper.Map<Project>(projectDTO);
            projectEntity.CreatedAt = System.DateTime.Now;
            _context.Add(projectEntity);
            await _context.SaveChangesAsync();

            var createdProject = await _context.Projects
               .Include(project=>project.Author)
               .FirstAsync(project => project.Id == projectEntity.Id);

            var createdProjectDTO = _mapper.Map<ProjectDTO>(createdProject);

            return createdProjectDTO;
        }
        public async System.Threading.Tasks.Task UpdateProject(ProjectDTO projectDTO)
        {
            Project projectEntity = await GetProjectByIdInternal(projectDTO.Id);

            if (projectEntity != null)
            {
                projectEntity.Name = projectDTO.Name;
                projectEntity.Description = projectDTO.Description;

                _context.Projects.Update(projectEntity);
                await _context.SaveChangesAsync();
            }
        }

        public async System.Threading.Tasks.Task DeleteProject(int id)
        {
            var projectEntity = await GetProjectByIdInternal(id);
            if(projectEntity!=null)
            {
                _context.Projects.Remove(projectEntity);
                await _context.SaveChangesAsync();
            }
        }


        private async Task<Project> GetProjectByIdInternal(int id)
        {
            return await _context.Projects
              .Include(project => project.Author)
              .Include(project => project.Tasks)
              .ThenInclude(task => task.Performer)
              .Include(project => project.Team)
              .FirstOrDefaultAsync(project=>project.Id==id);
              
        }

    }
}
